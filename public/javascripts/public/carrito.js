if (localStorage.carrito) {
	$('#cantidad_carrito').text(contar(localStorage.carrito));
} else {
	localStorage.setItem("carrito", "");
	$('#cantidad_carrito').text(contar(localStorage.carrito));
}

function agregar_carrito(componente) {

	if (contar(localStorage.carrito) == 0) {
		var a = [{
			id: parseInt(componente.id),
			stock: 1
		}]
		localStorage.setItem("carrito", JSON.stringify(a));
		swal.fire(
			'Producto Agregado!',
			'',
			'success'
		)
		$('#cantidad_carrito').text(contar(localStorage.carrito));
	} else {
		console.log("no es igual a cero ");
		var a = JSON.parse(localStorage.carrito);
		var resultado = [];
		a.forEach(function(element,index){
			console.log(element);
			if (element.id == componente.id) {
				console.log("Es el mismo Producto");
				var b = {
					id: parseInt(componente.id),
					stock: parseInt(element.stock) +1
				}
				console.log(b)

			} else {
				console.log("Productos Diferentes");
				var b = {
					id: parseInt(componente.id),
					stock: 1
				}
				console.log(b);
			}
		}); 
	}
}

function contar(array)

{
	if (array == "") {
		b = 0;
		return b;
	} else {
		b = JSON.parse(array);
		return b.length;
	}

}