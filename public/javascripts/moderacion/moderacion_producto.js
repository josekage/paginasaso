function aprobar(_id, _correo)
{
	console.log("Aprobando el producto")
	
	$.post('/api/moderacion/aprobar',{id_producto:_id, correo: _correo}, function(dt){
		if(dt=="success")
		{
			swal.fire("El Producto fue aprobado").then(function(){
				location.href="/moderacion"
			})
		}
	})
}

function rechazar(_id, _correo)
{
	$.post('/api/moderacion/rechazar',{id_producto:_id, correo: _correo}, function(dt){
		if(dt=="success")
		{
			swal.fire("El Producto fue negado").then(function(){
				location.href="/moderacion"
			})
		}
	})
}