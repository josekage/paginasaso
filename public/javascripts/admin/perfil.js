var progress = $('#contenedor_progress');
var foto_estudiante ="";
var input_imagen = $('#imagen');
var contenedor_imagen =$('#contenedor_imagen');
var foto = $('#foto')
var nueva_foto;
var data_colegios = new Array();
progress.hide();
$('select').material_select();
// Script para subir imagen 

input_imagen.change(function(data){
	var a = $('#form_imagen');
	formData = new FormData(a[0]);
	$.ajax({
		  url: '/api/subir-imagen',
		  type: 'POST',
		  data: formData,
		  processData: false,
		  contentType: false,
		  success: function(data){
			  console.log('la ruta es ');
			  console.log(data);
			  nueva_foto=data;
			  foto_estudiante = data;
			  foto.attr("src", data);
			  progress.hide();
			 
		  }
});
});
// script para guardar los datos ingresados 
var UsuarioGlobal={}
$('#btnActualizar').click(function(){
	var loading_screen = pleaseWait({
	  logo: "/images/assets/wait.png",
	  backgroundColor: '#F9FCF6',
	  loadingHtml: "<div class='indeterminate'></div>"
	});

		UsuarioGlobal.nombre_usuario =$('#nombre_usuario').val(),
		UsuarioGlobal.ci_usuario = $('#ci').val(),
		UsuarioGlobal.telefono_usuario = $('#celular').val(),
		UsuarioGlobal.ciudad_usuario = $('#ciudad').val(),
		UsuarioGlobal.foto_usuario= nueva_foto
		//negocio
		UsuarioGlobal.negocio={}
		UsuarioGlobal.negocio.nombre= $('#nombreNegocio').val()
		UsuarioGlobal.negocio.actividad= $('#actividadNegocio').val()
		UsuarioGlobal.negocio.telefono= $('#telefonoNegocio').val()
		UsuarioGlobal.negocio.correo= $('#correoNegocio').val()
		UsuarioGlobal.negocio.lat = $('#Negolat').val()
		UsuarioGlobal.negocio.lng = $('#Negolng').val()

	
	$.post('/api/actualizar-usuario',UsuarioGlobal,function(result){
		console.log(result);
		if(result=="success")
			{
				loading_screen.finish();
			}
		else{
				loading_screen.finish();
		}
	});
	
	
});


//FUnciones auxiliares
function noAction(){
	
}

function addMarker(data,data2){
	console.log(data)
	console.log(data2)
	$('#Negolat').val(data)
	$('#Negolng').val(data2)
}
//MAPA


function initMap() {
	const myLatlng = { lat: -1.2481486, lng: -78.6251787 };
	const map = new google.maps.Map(document.getElementById("map"), {
	  zoom: 15,
	  center: myLatlng,
	});
	// Create the initial InfoWindow.
	let infoWindow = new google.maps.InfoWindow({
	  content: "Dale click conde deseas agregar tu negocio",
	  position: myLatlng,
	});

	infoWindow.open(map);


	map.addListener("click", (mapsMouseEvent) => {
	  infoWindow.close();
	  infoWindow = new google.maps.InfoWindow({
		position: mapsMouseEvent.latLng,
	  });
	  var coor=mapsMouseEvent.latLng.toJSON()
	  console.log(coor)
	  var lat=coor.lat
	  var lng=coor.lng

	  infoWindow.setContent(
		//JSON.stringify(mapsMouseEvent.latLng, null, 2)
		`<h6>¿Quieres agregar tu negocio en esas Coordenadas?</h6>
		<br>
		<button class="waves-effect waves-light btn blue" onclick="addMarker('${lat}','${lng}')" >SI</button>
		<button class="waves-effect waves-light btn red"  onclick="${infoWindow.close()}"  >NO</button>
		`
	  );
	  infoWindow.open(map);
	});

	

}