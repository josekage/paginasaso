function eliminar(componente) {
	console.log(componente);
	$.post('/api/eliminar-producto', {
		id: componente.id
	}, function (result) {
		console.log(result)
		if (result == "success") {
			swal.fire(
				'Producto eliminado!',
				'Tambien se elimino todas las preguntas relacionadas a este producto!',
				'success'
			).then(function(){
				window.location.href = "/mis-productos";
			});
			
		} else {
			swal.fire(
				'Lo Sentimos!',
				'Algo salio mal estamos trabajando en ello !',
				'error'
			)

		}
	})
}