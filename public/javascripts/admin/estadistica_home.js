

$( document ).ready(function() {
    $.get('/api-estadisticas-producto',function(el){
        console.log(el)
        var nombres=[]
        var visitas=[]
        var contactos=[]
        el.forEach(function(producto){
            nombres.push(producto.nombre)
            visitas.push(producto.visitas)
            contactos.push(producto.contacto)
        })
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Mis Productos'
            },
            xAxis: {
                categories: nombres
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Popularidad de mis productos'
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                shared: true
            },
            plotOptions: {
                column: {
                    stacking: 'percent'
                }
            },
            series: [
                {
                    name: 'Contactos',
                    data: contactos
                },
                {
                name: 'Visitas',
                data: visitas
            }]
        });
    
    })
});