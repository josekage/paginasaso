var categoria;
var fotografias = [];
var f_originales = [];
var n_fotos = [];
Dropzone.autoDiscover = false;
//datos para subir 
//categoria nombre precio descripcion fotos
//
$('#crear').click(function () {
	n_fotos = [];
	//crear vector de imagen 
	for (i = 0; i < fotografias.length; i++) {
		if (fotografias[i]) {
			n_fotos.push(fotografias[i]);
		}
	}
	var nombre = $('#nombre').val();
	var precio = $('#precio').val();
	var descripcion = $('#descripcion').val();
	var f1 = n_fotos[0];
	var f2 = n_fotos[1];
	var f3 = n_fotos[2];
	var f4 = n_fotos[3];

	
	if (n_fotos.length == 0) {
		swal.fire(
			'Oops...',
			'Debes subir al menos una fotografía!',
			'error'
		)
	} else {
		if (categoria && precio && descripcion) {
			var loading_screen = pleaseWait({
				logo: "/images/assets/wait.png",
				backgroundColor: '#F9FCF6',
				loadingHtml: "<div class='indeterminate'></div>"
			});
			$('#crear').hide();
			//Creando Producto 
			var producto = {
				nombre: nombre,
				descripcion: descripcion,
				precio: precio,
				f1: f1,
				f2: f2,
				f3: f3,
				f4: f4,
				fotos:JSON.stringify(n_fotos),
				categoria: categoria
			}
			$.post('/api/crear-producto', producto, function (result) {
				console.log(result);
				if (result == "success") {
					window.location.href = "/mis-productos";
				} else {
					loading_screen.finish()
					swal.fire(
						'lo sentimos...',
						'Estamos trabajando para sustentar este problema!',
						'error'
					)
				}
			});

		} else {
			swal.fire(
				'Oops...',
				'Debes Llenar todos los datos!',
				'error'
			)
		}
	}
});

function subcategorias(index) {
	console.log(index);
	$.post('/api/categorias', {
		id: index
	}, function (result) {
		$('#contenedor').remove();
		var html = '<div class="grey lighten-3 collection" id="contenedor">';
		result.forEach(function (element) {
			html += '<a  class="collection-item waves-effect waves-light green white-text" onclick="seleccionar_categoria(this.id)" id="' + element._id+ '"</">' + element.nombre+ "</a>";
		});
		html += "</div>";
		$('#c2').append(html);
	});
};

function step2(index) {
	categoria = index;
	
}





$(document).ready(function () {
	console.log('drop');



	var dropzone = new Dropzone('#demo-upload', {
		url: '/api/subir-imagen-producto',
		paramName: "file", // The name that will be used to transfer the file
		maxFiles: 4,
		addRemoveLinks: true,
		thumbnailMethod: 'contain',
		thumbnailWidth: 200,
		thumbnailHeight: 100,
		maxFilesize: 5, 
		removedfile: function (file) {

			var thum = file.previewElement;
			thum.remove();
			var img = file.upload.filename;
			console.log(img)
			f_originales.forEach(function (element, index) {
				console.log(element);
				if (img == element) {
					delete fotografias[index];
				}
			});
		},
		success: function (file, response) {
			console.log("success Upload")
			console.log(response);
			fotografias.push(response);
			f_originales.push(file.upload.filename);
		},
		accept: function (file, done) {
			console.log("accept file")
			if (file.name) {
				done();
				console.log(file.name);

			} else {
				done();
			}
		}
	  });	


	
});

function seleccionar_categoria(element) {
	console.log(element);
	categoria = element;
	window.scroll(0,500) ;
}

console.log("script!")