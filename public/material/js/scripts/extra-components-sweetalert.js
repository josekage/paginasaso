/*
 * Sweet Alerts - Extra Components
 */

$(function() {
	"use strict";

	$('.btn-message').click(function() {
		swal.fire("Here's a message!");
	});

	$('.btn-title-text').click(function() {
		swal.fire("Here's a message!", "It's pretty, isn't it?")
	});

	$('.btn-timer').click(function() {
		swal.fire({
			title: "Auto close alert!",
			text: "I will close in 2 seconds.",
			timer: 2000,
			showConfirmButton: false
		});
	});

	$('.btn-success').click(function() {
		swal.fire("Good job!", "You clicked the button!", "success");
	});

	$('.btn-warning-confirm').click(function() {
		swal.fire({
				title: "Are you sure?",
				text: "You will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes, delete it!',
				closeOnConfirm: false
			},
			function() {
				swal.fire("Deleted!", "Your imaginary file has been deleted!", "success");
			});
	});

	$('.btn-warning-cancel').click(function() {
		swal.fire({
				title: "Are you sure?",
				text: "You will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: "No, cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					swal.fire("Deleted!", "Your imaginary file has been deleted!", "success");
				} else {
					swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
				}
			});
	});

	$('.btn-custom-icon').click(function() {
		swal.fire({
			title: "Sweet!",
			text: "Here's a custom image.",
			imageUrl: 'images/favicon/apple-touch-icon-152x152.png'
		});
	});

	$('.btn-message-html').click(function() {
		swal.fire({
			title: "HTML <small>Title</small>!",
			text: 'A custom <span style="color:#F8BB86">html<span> message.',
			html: true
		});
	});

	$('.btn-input').click(function() {
		swal.fire({
				title: "An input!",
				text: 'Write something interesting:',
				type: 'input',
				showCancelButton: true,
				closeOnConfirm: false,
				animation: "slide-from-top",
				inputPlaceholder: "Write something",
			},
			function(inputValue) {
				if (inputValue === false) return false;

				if (inputValue === "") {
					swal.showInputError("You need to write something!");
					return false;
				}

				swal.fire("Nice!", 'You wrote: ' + inputValue, "success");

			});
	});

	$('.btn-theme').click(function() {
		swal.fire({
			title: "Themes!",
			text: "Here's the Twitter theme for SweetAlert!",
			confirmButtonText: "Cool!",
			customClass: 'twitter'
		});
	});

	$('.btn-ajax').click(function() {
		swal.fire({
			title: 'Ajax request example',
			text: 'Submit to run ajax request',
			type: 'info',
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		}, function() {
			setTimeout(function() {
				swal.fire('Ajax request finished!');
			}, 2000);
		});
	});

});