
var commerces=[
  // {
  //   name:"Elam Shoes",
  //   lat:-1.2357497, 
  //   lng:-78.6236467,
  //   description:"Calle Bolivar frente a TIA"
  //   // img:'/comercios/feriaCalzado.jpeg'
  // },
  // {
  //   name:"Magical and Special Moments",
  //   lat:-1.2394847,
  //   lng:-78.6294532,
  //   // img:'/comercios/feriaCalzado.jpeg',
  //   description:'Martinez entre Rocafuerte y Cuenca'
  // },
  {
    name:"Café Chocolate Cafetería",
    lat:-1.3965994,
    lng:-78.4240559,
    // img:'/comercios/feriaCalzado.jpeg',
    description:'Oriente y Thomás Halflans'
  },
  {
    name:"Soluciones Nahova",
    lat:-1.3966452,
    lng:-78.4227782,
    // img:'/comercios/feriaCalzado.jpeg',
    description:'Oriente y Eloy Aflaro '
  },
  {
    name:"Tienda de Regalos Carolina Carrillo",
    lat:-1.3965994,
    lng:-78.4240559,
    // img:'/comercios/feriaCalzado.jpeg',
    description:"Tomas Halflans y Oriente"
  },
  {
    name:"La Casa del Celular",
    lat:-0.9316582,
    lng:-78.6198482,
    // img:'/comercios/feriaCalzado.jpeg',
    description:"5 de junio y avenida Amazonas"
  },
 
  {
    name:"Café de Vero",
    lat:-0.9318523,
    lng:-78.6155552,
    // img:'/comercios/feriaCalzado.jpeg',
    description:"Sanchez de Orellana y Guayaquil"
  },
  {
    name:"Parrilladas Mama Miche",
    lat:-0.9200426,
    lng:-78.6207244,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Bassantes Camino Paulina",
    lat:-0.9351552,
    lng:-78.6164905,
    // img:'/comercios/feriaCalzado.jpeg', 
    description:"General Maldonado y Tarqui"
  },
  //JOSE
  {
    name:"Brissa y mar",
    lat:-1.4797722,
    lng:-78.0021118,
    // img:'/comercios/feriaCalzado.jpeg',
    description:"Avenida 20 de julio y Tungurahua"
  },
  {
    name:"Peter Cell",
    lat:-0.2716599,
    lng:-78.5368099,
    // img:'/comercios/feriaCalzado.jpeg',
    description:"ALLE AYAPAMBA OE 2 - 06 y, Ave Teniente Hugo Ortiz, Quito 090515"
  },
  {
    name:"Mil Espigas",
    lat:-1.4787078,
    lng:-78.0000404,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"El Espigal",
    lat:-0.99236439,
    lng:-77.8143086,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Café Chocolate",
    lat:-1.3966615,
    lng:-78.4232217,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Papeleria Ericka ",
    lat:-1.0032505,
    lng:-77.8111785,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Tercena Farrukito ",
    lat:-0.9879519,
    lng:-77.8153057,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:" Christopeher Moncayo",
    lat:-1.1681175,
    lng:-78.5465159,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Casa del celular ",
    lat:-0.9318756,
    lng:-78.6184828,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    //no existe calle transveral
    name:" Moncayo Irene",
    lat:-1.168978,
    lng:-78.5466077,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:" Café de Vero ",
    lat:-0.9318065,
    lng:-78.6150623,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:" Barriga David",
    lat:-1.1726462,
    lng:-78.5469464,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Casillas Kleber",
    lat:-0.9318756,
    lng:-78.6184828,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:" Galora Angel",
    lat:-1.2050257,
    lng:-78.5419411,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  //no existe
  // {
  //   name:" Araujo Carolina",
  //   lat:-1.2050257,
  //   lng:-78.5419411,
  //   // img:'/comercios/feriaCalzado.jpeg'
  // },
  {
    name:" Pazuña Maximilianno",
    lat:-1.1721982,
    lng:-78.5451117,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Luisa Perrazo ",
    lat:-1.3306586,
    lng:-78.5443699,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:" Maria Barrionuevo",
    lat:-1.3120499,
    lng:-78.5116676,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Farmacia Cruz Azul ",
    lat:-1.2050257,
    lng:-78.5419411,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Farmacia Cruz Azul 2",
    lat:-1.2050257,
    lng:-78.5419411,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Brisas y Mar 1 ",
    lat:-1.6641605,
    lng:-78.6495997,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Brisas y Mar 2 ",
    lat:-1.6782561,
    lng:-78.6561957,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  {
    name:"Brisas y Mar 3 ",
    lat:-1.6722731,
    lng:-78.6561923,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  //no existe local
  {
    name:"Dermacentere",
    lat:-1.4914151,
    lng:-77.9961615,
    // img:'/comercios/feriaCalzado.jpeg'
  },

  {
    name:"Parrilladas Mama Miche ",
    lat:-0.9272961,
    lng:-78.6189594,
    // img:'/comercios/feriaCalzado.jpeg'
  },
  //NUEVAS JOSE
  {
    name:"ELAM`S shoes",
    lat:-1.2396303,
    lng:-78.6272432,
    description:"Simón Bolívar 1442 ",
    telefonos: "097936234-0957327076",
    actividad: "Calzado Brasilero",
    img:'/comercios/elan.jpeg'
  },
  {
    name:"Frigorífico popular ",
    lat:-1.2401401,
    lng:-78.6262527,
    ciudad:"Ambato",
    description:"Mercado Modelo, Av Cevallos",
    telefonos: "097666234-0979829576",
    actividad: "Carniceria",
    img:'/comercios/popular.jpeg'
  },
  {
    name:"Comercial Gutiérrez",
    lat:-1.2402366,
    lng:-78.6270896,
    ciudad:"Ambato",
    description:"Sucre entre Joaquin Lalama y Mariano Eguez",
    telefonos: "0976412634-0957679576",
    actividad: "Venta de material eléctrico ",
    img:'/comercios/gutierrez.jpeg'
  },
  {
    name:"Magic Special",
    lat:-1.2396684,
    lng:-78.6288611,
    ciudad:"Ambato",
    description:"Martínez 16 entre Vicente Rocafuerte y Cuenca",
    telefonos: "0976412634-0957679576",
    actividad: "Detalles de fiestas",   
    img:'/comercios/magic.jpeg'
  },



]

var map;
  	 function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: -1.173213, lng: -78.542285},
          zoom: 9,
        });
       
        var markers=[]
       commerces.forEach((commerce, index)=>{
         var auxMarker=new google.maps.Marker({

          position: {lat: commerce.lat, lng: commerce.lng},
          map: map,
          title: commerce.name
          
         })

         auxMarker.addListener("click", () => {

            $('#modal1').modal();
            $('#modal1').modal('open'); 
            $('#modalTitle').text(commerce.name)
            $('#description').text(commerce.description)
            $('#ciudad').text(commerce.ciudad)
            $('#telefonos').text(commerce.telefonos)
            $('#actividad').text(commerce.actividad)
            $('#commerceImg').attr('src', commerce.img);
          });
         

         markers.push(auxMarker)
       })

      //  console.log(markers)
        // var marker = new google.maps.Marker({
        //   position: {lat: -1.243772, lng: -78.629029},
        //   map: map,
	      //   title: 'Smart Ecuador'
        // });

        // marker.addListener("click", () => {
        //     console.log("Smart Ecuador ")
        //     $('#modal1').modal();
        //     $('#modal1').modal('open'); 
        //   });
      }
 