var LocalStrategy = require('passport-local').Strategy;
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
const { user } = require('../database/config');

usuario_model= require('../models/usuario')

var FacebookStrategy = require('passport-facebook').Strategy;

//lo siguiente es el modulo que importa a la app 
module.exports = function(passport){

	passport.serializeUser(function(user, done){
		done(null, user);
	});

	passport.deserializeUser(function(obj, done){
		done(null, obj);
	});

	passport.use(new LocalStrategy({
		passReqToCallback : true
	}, function(req, email, password, done){

		

		usuario_model.find({activo:"1", correo:email},function(err,docs){
			if(docs.length > 0){

				var user = docs[0];
				// aqui se define la variable user que se maneja en todas las vistas en las que haga login
				if(bcrypt.compareSync(password, user.password)){
					return done(null, {
						id: user._id, 
						nombre : user.nombre,
						telefono:user.celular,
						email : user.correo,
						tipo : user.tipo,
						ciudad :user.ciudad,
						foto: user.foto,
						ci: user.ci, 
						tipo:user.tipo
					});
				}
				else
				{
					return done(null, false);
				}
			}
			else
			{
				return done(null, false);
			}
		})

		// db.query('SELECT * FROM usuario  WHERE  correo_usuario = ?', email, function(err, rows, fields){
		// 	if(err) console.log(err);

		// 	db.end();

		// 	if(rows.length > 0){

		// 		var user = rows[0];
		// 		// aqui se define la variable user que se maneja en todas las vistas en las que haga login
		// 		if(bcrypt.compareSync(password, user.password_usuario)){
		// 			return done(null, {
		// 				id: user.id_usuario, 
		// 				nombre : user.nombre_usuario,
		// 				telefono:user.telefono_usuario,
		// 				email : user.correo_usuario,
		// 				tipo : user.tipo_usuario,
		// 				ciudad :user.ciudad_usuario,
		// 				foto: user.foto_usuario,
		// 				ci: user.ci_usuario, 
		// 				tipo:user.tipo_usuario
		// 			});
		// 		}
		// 	}

		// 	return done(null, false, req.flash('authmessage', 'Email o Password incorrecto.'));

		// });

	}
	));

	//facebook Login 
	passport.use(new FacebookStrategy({
        //Ingresamos las credenciales de nuestra aplicacion 
        clientID: '2502467796749846',
        clientSecret: '15b31f812394e415a0ed64eaaeda7033',
        callbackURL: "/callback/facebook",// se selecciona a donde redirige el momento de que termine el proceso
        passReqToCallback: true
      },
      function(req,accessToken, refreshToken, profile,done) {
        console.log("Datos desde facebook ")
        console.log(profile)
        //buscamos si hay un usuario con este fid dentro de nuestra base si no hay 
        //crea un nuevo usuario, en este caso no hace falta tener password 
        usuario_model.find({fid:profile.id}, function(err,docs){
            console.log("buscando usuario facebook ")
            console.log(docs)
            console.log("*******")
            if(docs.length==0)
            {
                console.log("Usuario no existe")
                obj= new usuario_model({
                    activo:"1",
                    nombre:profile.displayName,
                    fid:profile.id,
                })
                obj.save(function(err,docs){
                    console.log(err)
                    console.log(docs)
                    return done(null, {
                        id: docs._id,
						nombre:docs.nombre,
						fid:profile.id,
						tipo:docs.tipo,
                        completed:docs.completed,
                        negocio:docs.negocio
                    });
                })
            }
            else
            {
                console.log("Elusuario ya esta registrado mediante facebook ")
                var user = docs[0]
             return  done(null, {
                    id: user._id,
                    nombre : user.nombre,
                    correo : user.correo,
                    foto:user.foto,
                    ciudad:user.ciudad,
					ci:user.ci,
					tipo:user.tipo,
					fid:profile.id,
                    direccion:user.direccion,
                    celular:user.celular,
					completed:user.completed,
					negocio:user.negocio
                });
            }
            
        })  
       
       
      }
    ));


	//fin Facebook 


};