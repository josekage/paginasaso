var express = require('express');
var router = express.Router();
var passport = require('passport');
var controllers = require('.././controllers');
var AuthMiddleware = require('.././middleware/auth');
//var formidable = require('express-formidable');
var fs = require('fs');

//Public
router.get('/', controllers.UserControllers.home);
router.get('/login',AuthMiddleware.isNotLogin, controllers.UserControllers.login);
router.post('/',  passport.authenticate('local', {
	successRedirect : '/panel',
	failureRedirect : '/login',
	failureFlash : true 
}));

//login Facebook 
router.get('/auth/facebook', passport.authenticate('facebook'));

router.get('/callback/facebook', passport.authenticate('facebook', {failureRedirect: '/login?success=false' }), function(req,res,next){
    //console.log(req.query)
    
    return res.redirect('/panel')
	
});

//Fin Login Facebook 

router.get('/panel', AuthMiddleware.isLogged,AuthMiddleware.isNotCompleted , controllers.AdminControllers.panel);
router.get('/salir',AuthMiddleware.isLogged, controllers.AdminControllers.logout);
router.get('/carrito',controllers.UserControllers.carrito);
router.get('/producto/:id',controllers.UserControllers.producto);
router.get('/categoria/:id', controllers.UserControllers.categoria);
// admin 
router.get('/mi-perfil', AuthMiddleware.isLogged, controllers.AdminControllers.perfil);
router.get('/nuevo-producto', AuthMiddleware.isLogged, controllers.AdminControllers.getNuevoProducto);
router.get('/mis-productos', AuthMiddleware.isLogged, controllers.AdminControllers.getMisProductos);
router.get('/favoritos', AuthMiddleware.isLogged, controllers.AdminControllers.getFavoritos);
router.get('/quitar-favoritos', AuthMiddleware.isLogged, controllers.AdminControllers.getQuitarFavoritos);

//API 
router.post('/api/subir-imagen',AuthMiddleware.isLogged,controllers.ApiControllers.postSubirImagen);
router.post('/api/subir-imagen-producto',AuthMiddleware.isLogged,controllers.ApiControllers.postSubirImagenProducto);
router.post('/api/actualizar-usuario',AuthMiddleware.isLogged,controllers.AdminControllers.postActualizarPerfil);
router.post('/api/categorias',controllers.ApiControllers.postCategorias);
router.post('/api/borrar/imagen-producto',AuthMiddleware.isLogged,controllers.ApiControllers.postBorrarImagenProducto);
router.post('/api/crear-producto', AuthMiddleware.isLogged,controllers.ApiControllers.postCrearProducto);
router.post('/api/eliminar-producto', AuthMiddleware.isLogged, controllers.ApiControllers.postEliminarProducto);
router.post('/api/productos', controllers.ApiControllers.postProductos);
router.post('/api/info-producto', controllers.ApiControllers.postInfoProducto);
router.post('/api/nuevo-favorito', AuthMiddleware.isLogged, controllers.ApiControllers.postNuevoFavorito);

router.post('/api/nueva-pregunta', AuthMiddleware.isLogged, controllers.ApiControllers.postNuevaPregunta);
router.post('/api/nueva-resena', AuthMiddleware.isLogged, controllers.ApiControllers.postNuevaResena);

router.get('/buscar-:name',controllers.UserControllers.buscar);

router.get('/moderacion', AuthMiddleware.isAdmin, controllers.ModeracionController.getPanelModeracion);
router.get('/moderacion-:id_producto', AuthMiddleware.isAdmin, controllers.ModeracionController.getDescripcionProducto);
router.post('/api/moderacion/aprobar', AuthMiddleware.isAdmin, controllers.ModeracionController.ApiAprobar);
router.post('/api/moderacion/rechazar', AuthMiddleware.isAdmin, controllers.ModeracionController.ApiRechazar);

//USUARIO

router.get('/usuarios',AuthMiddleware.isAdmin,controllers.ModeracionController.getListadoUsuarios)
router.get('/nuevo-usuario',AuthMiddleware.isAdmin,controllers.ModeracionController.getNuevoUsuario)
router.post('/nuevo-usuario',AuthMiddleware.isAdmin,controllers.ModeracionController.postNuevoUsuario)
router.get('/usuarios-editar',AuthMiddleware.isAdmin,controllers.ModeracionController.getEditarUsuario)
router.post('/usuarios-editar',AuthMiddleware.isAdmin,controllers.ModeracionController.postEditarUsuario)

router.get('/activar-usuario', AuthMiddleware.isAdmin, controllers.ModeracionController.getActivarUsuario)
router.get('/desactivar-usuario', AuthMiddleware.isAdmin, controllers.ModeracionController.getDesactivarUsuario)
//preguntas

router.get('/responder', AuthMiddleware.isLogged,controllers.AdminControllers.getResponder)
router.post('/responder', AuthMiddleware.isLogged,controllers.AdminControllers.postResponder)

//registro

router.get('/registro',AuthMiddleware.isNotLogin,controllers.UserControllers.getRegistro)
router.post('/registro',AuthMiddleware.isNotLogin,controllers.UserControllers.postRegistro)

router.get('/confirmar-correo',AuthMiddleware.isNotLogin, controllers.UserControllers.getConfirmarCorreo)

//recuperar COntraseña
router.get('/recuperar-password', controllers.UserControllers.getRecoverPassword)
router.post('/recuperar-password', controllers.UserControllers.postRecoverPassword )
router.get('/set-password',controllers.UserControllers.getSetPassword)
router.post('/set-password',controllers.UserControllers.postSetPassword)


//Promociones
router.get('/borrar-promo', AuthMiddleware.isAdmin, controllers.PromoController.getDeletePromo)
router.get('/administrar-promociones', AuthMiddleware.isAdmin, controllers.PromoController.getAdminPromo)
router.post('/administrar-promociones', AuthMiddleware.isAdmin, controllers.PromoController.postAdminPromo)
router.get('/beneficios',controllers.PromoController.getBeneficios)

router.get('/send-mail-noSocio',AuthMiddleware.isAdmin, controllers.ModeracionController.getNoSocio)
router.get('/send-mail-infoDesactualizada',AuthMiddleware.isAdmin,controllers.ModeracionController.getInfoDesactualizada)

//Estadisticas

router.get('/api-estadisticas-producto', AuthMiddleware.isLogged,controllers.ApiControllers.getStats)
router.get('/api-estadisticas-contacto',controllers.ApiControllers.getApiContacto)

//API

// router.get('/api/global-estadisticas', controllers.ApiControllers.getGlobalStats);

//Dashboard
router.get('/dashboard', AuthMiddleware.isAdmin, controllers.AdminControllers.getDashBoard)

router.get('/validar-datos',AuthMiddleware.isLogged ,controllers.UserControllers.getValidarDatos)
router.post('/validar-datos',AuthMiddleware.isLogged ,controllers.UserControllers.postValidarDatos)



///GEOREFERENCIACION 

router.get('/comercios', controllers.ComercioController.getComercio)

module.exports = router;
