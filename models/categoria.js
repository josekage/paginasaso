var mongoose = require ('mongoose');
	Schema = mongoose.Schema;
var categoriaSchema = new Schema({
	nombre: {type:String},
	categoria_padre:{type:String},
	indice:{type:Number},
	indice_padre:{type:Number},
})

module.exports = mongoose.model('Categoria', categoriaSchema);