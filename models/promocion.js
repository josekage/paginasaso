var mongoose = require ('mongoose');
	Schema = mongoose.Schema;
var promocionSchema = new Schema({
	nombre: {type:String},
	beneficio: {type:String},
	direccion:{type:String},
	telefono:{type:String},
	imagen:{type:String},
	web:{type:String},
	ciudad:{type:String},
	visible:{type:String, default:"1"},
	f_creacion: {type:Date, default: Date.now}
})

module.exports = mongoose.model('Promo', promocionSchema);