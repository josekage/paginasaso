var mongoose = require ('mongoose');
	Schema = mongoose.Schema;

var usuarioSchema = new Schema({
	nombre: {type:String},
	correo: {type:String},
	tipo: {type:String, default:"2"},
	confirmacion: {type:String, default:"0"},
	password: {type:String},
	celular: {type:String},
	agencia: {type:String},
	grupo: {type:String},
	ciudad: {type:String},
	cod: {type:String},
	ci: {type:String},
	foto:{type:String},
	fid:{type:String},
	geo:{type:Object},
	negocio:{type:Object, default:[]},
	activo:{type:String, default:"0"},//0 inactivo , 1 Activo, 3 rechazado No es socio, 
	completed:{type:String, default:"0"},//0 inactivo , 1 Activo,
	f_creacion: {type:Date, default: Date.now},
	f_oscus: {type:Date},

})

module.exports = mongoose.model('Usuario', usuarioSchema);