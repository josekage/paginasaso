var mongoose = require ('mongoose');
	Schema = mongoose.Schema;
var preguntaSchema = new Schema({
	pregunta: {type:String},
	producto: {type:String, ref:"Producto"},
	id_usuario: {type:String, ref:"Usuario"},
	usuario: {type:Object, },
	f_creacion: {type:Date, default: Date.now}
})
//activo es cuando es producto esta creado o eliminado 

//publicado es cuando esta publicado o denegado 
module.exports = mongoose.model('Pregunta', preguntaSchema);