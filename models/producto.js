var mongoose = require ('mongoose');
	Schema = mongoose.Schema;
var productoSchema = new Schema({
	nombre: {type:String},
	categoria: {type:String},
	
	foto_1: {type:String},
	foto_2: {type:String},
	foto_3: {type:String},
	foto_4: {type:String},
	
	fotos: {type:Object},

	slug: {type:String},
	vendedor: {type:String, ref:"Usuario"},
	descripcion:{type:String},
	activo:{type:String , default:"1"},
	publicado:{type:String , default:"1"},
    precio:{type:Number},
    visitas:{type:Number, default:0},
    contacto:{type:Number, default:0},
    
	f_creacion: {type:Date, default: Date.now}
})
//activo es cuando es producto esta creado o eliminado 

//publicado es cuando esta publicado o denegado 
module.exports = mongoose.model('Producto', productoSchema);