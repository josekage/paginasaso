var mongoose = require ('mongoose');
	Schema = mongoose.Schema;
var respuestaSchema = new Schema({
	respuesta: {type:String},
    producto: {type:String, ref:"Producto"},
    pregunta: {type:String, ref:"Pregunta"},
    id_usuario: {type:String, ref:"Usuario"},
	f_creacion: {type:Date, default: Date.now}
})
//activo es cuando es producto esta creado o eliminado 

//publicado es cuando esta publicado o denegado 
module.exports = mongoose.model('Respuesta', respuestaSchema);