var mongoose = require ('mongoose');
	Schema = mongoose.Schema;
var favoritoSchema = new Schema({
	
	usuario: {type:String, ref:"Usuario"},
	producto: {type:String, ref:"Producto"},
    
	f_creacion: {type:Date, default: Date.now}
})

module.exports = mongoose.model('Favorito', favoritoSchema);