var mongoose = require ('mongoose');
	Schema = mongoose.Schema;
var productoSchema = new Schema({
	propietario: {type:String, ref:"Usuario"},
	evaluador: {type:String, ref:"Usuario"},
	descripcion:{type:String},
	puntuacion:{type:Number},
	observacion:{type:String},
	f_creacion: {type:Date, default: Date.now}
})

module.exports = mongoose.model('Resena', productoSchema);