var formidable = require('express-formidable');
var promo_model=require('../models/promocion')
module.exports={
    getAdminPromo:function(req,res,next){
        promo_model.find({visible:"1"},function(err,docs){
            return res.render('promo/admin',{
                user:req.user,
                promos:docs
            })
        })
    },
    postAdminPromo:function(req,res,next){
       obj= new promo_model(req.body)
       obj.save(function(err,rows){
           return res.redirect('/administrar-promociones')
       })
    },

    getDeletePromo:function(req,res,next){
        promo_model.update({_id:req.query.cod},{visible:"0"},function(err,docs){
            return res.redirect('/administrar-promociones')
        })
    },
    getBeneficios:function(req,res,next){
        if(req.query.ciudad)
        {
            ciudad=req.query.ciudad
            promo_model.find({visible:"1",ciudad:ciudad.toUpperCase() },function(err,docs){
                return res.render('promo/beneficios',{
                    promos:docs
                })
            })
        }
        else
        {
            promo_model.find({visible:"1" },function(err,docs){
                return res.render('promo/beneficios',{
                    promos:docs
                })
            })
        }
       
    }
}