var request = require('request');
//controlador para notificaciones
var nodeoutlook = require('nodejs-nodemailer-outlook')

module.exports={

    RegistroRevisandoInformacion:function(email){
        //cuando ya confirmo el correo y aun no esta activa la cuenta 
        //console.log("Enviando Correo ")
        //send('d-5c75e7bed360417680ad36f4bfe95ef0',email)
        console.log("******************** ENVIANDO NOTIFICACION *******************")
        var html_template=require('../plantillas_email/VerficandoInformacion')
        var template=html_template.enviar()
        enviar_outlook(email,"[ANUNCIOS OSCUS] Estamos revisando tu información",template)
    },
    CuentaNoVerificadaXDatos:function(email)
    {
        //send('d-e16c979b48ea4e5c8673a00baf84a84e',email)
        console.log("******************** ENVIANDO NOTIFICACION *******************")
        var html_template=require('../plantillas_email/DatosDesactualizados')
        var template=html_template.enviar()
        enviar_outlook(email,"[Anuncios OSCUS] Por favor actualiza tus datos",template)
    },
    ProductoAprobado : function(email)
    {
      console.log("******************** ENVIANDO NOTIFICACION *******************")
      var html_template=require('../plantillas_email/productoAprobado')
      var template=html_template.enviar() 
      // send('d-8af2a89d567c45e8a25b12e39aba2fe4',email)
      enviar_outlook(email,"[Anuncios OSCUS] Felicidades tu Producto fué aprobado",template)
    },
    ProductoRecibido: function(email)//probado
    {
       console.log("******************** ENVIANDO NOTIFICACION *******************")
       var html_template=require('../plantillas_email/productoRecibido')
       var template=html_template.enviar()
       enviar_outlook(email,"[ANUNCIOS OSCUS] Tu producto fué recibido, estamos revisando tu producto", template)
      //   send('d-79f4fd033e91434aa733ef0b9012ad13',email)
    },
    ProductoRechazado : function(email)
    {
      console.log("******************** ENVIANDO NOTIFICACION *******************")
      var html_template=require('../plantillas_email/productoRechazado')
      var template=html_template.enviar()
      enviar_outlook(email,"[ANUNCIOS OSCUS] Tu producto fué rechazado", template)
      //   send('d-0a49eefd61fe4cc7b2f1b2bb3ca51e64', email)
    },
    NoSocio : function(email)
    {
         console.log("******************** ENVIANDO NOTIFICACION *******************")
         var html_template=require('../plantillas_email/NoSocio')
         var template=html_template.enviar()
         enviar_outlook(email,"[ANUNCIOS OSCUS] Se nuestro socio y Accede a muchos beneficios", template)
         //   send('d-f30918c354374600a10b66ff5257d50d',email)
        //d-f30918c354374600a10b66ff5257d50d
    },

    ConfirmarCorreo: function(email,confirm_url)//Verificado
    {
      console.log("******************** ENVIANDO NOTIFICACION *******************")
         var html_template=require('../plantillas_email/verificarCorreo')
         var template=html_template.enviar(confirm_url)
         var asunto="Hemos recibido tu petición por favor confirma tu correo electrónico"
         enviar_outlook(email,asunto,template)
    }



}


function send(id_template,correo){
    
console.log(id_template)
console.log(correo)
url="https://api.sendgrid.com/v3/mail/send"
form={
   "from":{
      "email":"info@oscus.coop",
      "name":"Anuncios OSCUS"
   },
   "reply_to": {
      "email": "info@oscus.coop",
      "name": "Información OSCUS"
    },
   "subject": ".",
   "personalizations":[
      {
         "to":[
            {
               "email":correo
            }
         ],
         "dynamic_template_data":{
           
            "Sender_Name":"OSCUS Cooperativa de Ahorro y Crédito",
            "Sender_Address":"Lalama 06-39 entre SUcre y Bolivar",
            "Sender_City":"Ambato",
            "Sender_State":"Tungurahua",
            "Sender_Zip":"180105",
            "url_confirmacion":"----"
            
          }
      }
   ],
   "template_id":id_template
}

headers= {
   'content-type': 'application/json',
   'Authorization': 'Bearer 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'
 }




request.post({url:url, json: form, headers:headers }, function(err,httpResponse,body){ 
   console.log(body)
   console.log('success')
})

}


function send_url(id_template,correo,confirm_url){
    
    console.log(id_template)
    console.log(correo)
    url="https://api.sendgrid.com/v3/mail/send"
    form={
       "from":{
          "email":"info@oscus.coop",
          "name":"Anuncios OSCUS"
       },
       "reply_to": {
          "email": "info@oscus.coop",
          "name": "Información OSCUS"
        },
       "subject": ".",
       "personalizations":[
          {
             "to":[
                {
                   "email":correo
                }
             ],
             "dynamic_template_data":{
               
                "Sender_Name":"OSCUS Cooperativa de Ahorro y Crédito",
                "Sender_Address":"Lalama 06-39 entre SUcre y Bolivar",
                "Sender_City":"Ambato",
                "Sender_State":"Tungurahua",
                "Sender_Zip":"180105",
                "url_confirmacion":confirm_url
                
              }
          }
       ],
       "template_id":id_template
    }
    
    headers= {
       'content-type': 'application/json',
       'Authorization': 'Bearer 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'
     }
    
    
    
    
    request.post({url:url, json: form, headers:headers }, function(err,httpResponse,body){ 
       console.log(body)
       console.log('success')
    })
    
    }


function enviar_outlook(correo, asunto,html){
   nodeoutlook.sendEmail({
      auth: {
          user: "info@oscus.coop",
          pass: "Cooperativa2023"
      },
      from: 'info@oscus.coop',
      to: correo,
      subject: asunto,
      html: html,
      text: asunto,
      replyTo: 'info@oscus.coop',
      onError: (e) => console.log(e),
      onSuccess: (i) => console.log(i)
  }
  );
}