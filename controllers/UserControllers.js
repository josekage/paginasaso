var url="http://anuncios.oscus.coop"
// var url="http://192.168.0.103"
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
//var express = require('express');
const sgMail = require('@sendgrid/mail');
var formidable = require('express-formidable');
var sanitizer = require('sanitizer');
var producto_model=require('../models/producto')
var categoria_model=require('../models/categoria')
var usuario_model=require('../models/usuario')
var pregunta_model=require('../models/pregunta')
var respuesta_model=require('../models/respuesta')
var resena_model=require('../models/resena')

var PUBLIC_KEY  = '6Lcl-KsUAAAAAINv5iX2lrV9PRRV9GW_pRvG_5N2';
var PRIVATE_KEY = '6Lcl-KsUAAAAAJ9SC0d4kmTezW9PTWUBcLiG9sKv';
var Recaptcha = require('recaptcha-v2').Recaptcha;

var notify = require('../controllers/NotificacionController')
function send_mail(mail, mensaje, asunto){
	sgMail.setApiKey('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');
	const msg = {
	  to: mail,
	  from: 'info@oscus.coop',
	  subject: 'ANUNCIOS OSCUS',
	  text: asunto,
	  html: mensaje,
	};
	
	sgMail.send(msg);
	}
module.exports = {
    home : function(req, res , next)
    {
		producto_model.find({activo:"1", publicado:"1"},null,{sort:{f_creacion:-1},limit:24}, function(err,docs){
				if(err)
				{
					return res.render('public/error',{
						isAuthenticated: req.isAuthenticated(),
						user: req.user
					})
				}
				else
				{
					return res.render('public/home',{
						productos: docs,
						isAuthenticated: req.isAuthenticated(),
						user: req.user
					});
				}
		})
 
		
    },
	login : function(req,res,next)
	{
		return res.render('public/login',{
			msg:req.query.msg
		});
	},
	carrito: function(req,res,next)
	{
		return res.render('public/carrito',{
						isAuthenticated: req.isAuthenticated(),
						user: req.user
					});
	},
	producto: function(req,res,next)
	{
		var id = sanitizer.escape(req.params.id);
		console.log(id)
		producto_model.find({slug:req.params.id}, function(err,docs){
			console.log("retirno de la busqueda de producto")
			console.log(docs)
			if(err){
				console.log(err)
				return res.render('public/error',{
					isAuthenticated: req.isAuthenticated(),
					user: req.user
				})
			}
			else
			{

				console.log(docs.length)
				if(!docs.length>0)
				{

					return res.render('public/error',{
						isAuthenticated: req.isAuthenticated(),
						user: req.user
					})
				}
				else
				{
					
					
					usuario_model.populate(docs,{path:"vendedor"}, function(err2,docs2){
						categoria_model.populate(docs2, {path:"categoria"}, function(err3, docs3){
							producto_model.find({categoria:docs[0].categoria}, function (err4, related){
								if(!related)
								{
									related=[]
								}
								console.log(docs3[0]._id)
								pregunta_model.find({producto:docs3[0]._id}, function(err5, docs4){
									respuesta_model.find({producto:docs3[0]._id}, function(err6, respuestas){
										resena_model.find({propietario:docs3[0].vendedor._id}, function(err7,resena1){
											usuario_model.populate(resena1,{path:"evaluador"}, function(err8, resenas){
												
												// incrementando visitas
												producto_model.update({slug:req.params.id},{ $inc: { visitas: 1} },function(err9,incre){
													console.log(err9)
													console.log(incre)
													return res.render('public/producto',{
														producto: docs3[0],
														isAuthenticated: req.isAuthenticated(),
														user: req.user,
														relacionados: related,
														preguntas : docs4, 
														respuestas:respuestas,
														resenas:resenas
													});

												})
												

											})
										})
										
									})
									
								})
								
							})
							
						})
					})
					
				}
			}
		})
		// var config = require('.././database/config');
		// config.multipleStatements=true
		// var db = mysql.createConnection(config);
		// db.connect();
		// db.query('select * from producto inner join usuario on vendedor_producto = id_usuario inner join categoria2 on c2 = id_categoria2 where ?;', condicion, function (err, col, fields) {
		// 		if (err) {
		// 			console.log(err);
		// 			db.end();
		// 			return res.render('public/error',{
		// 				isAuthenticated: req.isAuthenticated(),
		// 				user: req.user
		// 			})
		// 		} else {
		// 			console.log(col)
		// 			db.query('select * from producto where id_producto !='+col[0].id_producto+' and c2 ='+col[0].c2+";  select * from pregunta  inner join usuario on usuario_pregunta = id_usuario where producto_pregunta ="+col[0].id_producto+";", function(err, rows, fields){
		// 				console.log(rows);
		// 				console.log(err);
		// 				db.end();
		// 			return res.render('public/producto',{
		// 				producto: col[0],
		// 				isAuthenticated: req.isAuthenticated(),
		// 				user: req.user,
		// 				relacionados: rows[0],
		// 				preguntas : rows[1]
		// 			});
					
						
		// 			})
					
		// 		}

		// 	});
		
	}, 
	categoria: function(req,res,next)
	{
		
		var id = sanitizer.escape(req.params.id);
		
		var id2=parseInt(id)
		console.log(id2)
		categoria_model.find({indice:id2},function(err,docs){
			console.log(err)
			console.log(docs)
			if(docs.length>0)
			{
				producto_model.find({categoria:docs[0]._id, activo:"1",publicado:"1"}, function(err2, docs2){
					if(docs2.length>0)
					{
						return res.render('public/categoria',{
							productos: docs2,
							isAuthenticated: req.isAuthenticated(),
							user: req.user,
							categoria:docs[0]
						});
					}
					else
					{
						return res.render('public/categoria',{
							productos: [],
							isAuthenticated: req.isAuthenticated(),
							user: req.user,
							categoria:docs[0]
						});
					}
					
				})
			}
			else
			{
				return res.render('public/categoria',{
					productos: [],
					isAuthenticated: req.isAuthenticated(),
					user: req.user,
					categoria:docs[0]
				});
			}
			
		})
		

	},
	material_test : function(req,res,next)
	{
		return res.render('horizontal_material');
	}, 

	buscar: function(req,res,next){
		var name = sanitizer.escape(req.params.name);
		producto_model.find({nombre: { $regex: '.*' + name + '.*' , $options:'i'}, activo:"1", publicado:"1" }, function(err,docs){
			return res.render('public/buscador',{
				productos: docs,
				isAuthenticated: req.isAuthenticated(),
				user: req.user
			});
		})
		}
	,

	getRegistro:function(req,res,next){
		return res.render('public/registro',{
			title:"registro"
		})
	}, 

	postRegistro : function(req,res,next){


		console.log(req.body)
		if(req.body['g-recaptcha-response']=='')
		{
			console.log("Recaptcha Vacia")
			return res.redirect("/login?msg=ES NECESARIO VERIFICAR QUE NO ERES UN ROBOT")
		}
		else
		{
			if((req.body['g-recaptcha-response']))
				{
					var data = {
						remoteip:  req.connection.remoteAddress,
						response:  req.body['g-recaptcha-response'],
						secret: PRIVATE_KEY
					};
					var recaptcha = new Recaptcha(PUBLIC_KEY, PRIVATE_KEY, data);
					
					recaptcha.verify(function(success, error_code) {
						console.log(error_code)
						if (success) {
			
							_pass=req.body.ci
							var salt = bcrypt.genSaltSync(10);
							req.body.cod = (Date.now() - Date.parse("January 01, 2015")).toString(32);
							req.body.password = bcrypt.hashSync(_pass, salt);
							var new_url=url+`/confirmar-correo?cod=`+req.body.cod
							notify.ConfirmarCorreo(req.body.correo,new_url)
							var obj= new usuario_model(req.body)
			
							obj.save(function(err,docs){
								return res.redirect("/login?msg=te hemos enviado un correo de confirmación")
							})
			
						}
						else
						{
							return res.redirect("/login?msg=ES NECESARIO VERIFICAR QUE NO ERES UN ROBOT")
						}
					
					})
				}
				else{
					return res.redirect("/login?msg=ES NECESARIO VERIFICAR QUE NO ERES UN ROBOT")
				}
		}
		
        

		
	},

	getConfirmarCorreo : function(req,res,next){
		usuario_model.find({cod:req.query.cod}, function(err,docs){
			console.log(docs)
			if(docs.length>0)
			{
				usuario_model.update({cod:req.query.cod},{confirmacion:"1"},function(err1,docs2){
					console.log(err1)
					console.log(docs2)
					notify.RegistroRevisandoInformacion(docs[0].correo)
					return res.redirect('/login?msg=Ya hemos verificado tu correo, en breve tu cuenta estará activa')
				})
			}
			else
			{
				return res.redirect('/login?msg=Algo salió mal, inténtalo de nuevo ')
			}
		})
	}, 

	getRecoverPassword : function(req,res,next){
		return res.render('public/recoveryPass',{
			title:"recordar Contraseña", 
			msg:req.query.msg
		})
	},

	postRecoverPassword : function(req,res,next){
		var correo = req.body.email.trim();
		correo=correo.toLowerCase()
		usuario_model.find({correo:correo}, function(err,docs){
			if(docs[0])
			{
				send_mail(docs[0].correo,`
				<h3>Recupera tu contraseña</h3>
				<p>Si no solicitaste esto, por favor ignora este mensaje</p>
				<br></br>
				<p>Para recuperar tu contraseña ingresa en el siguiente enlace</p>
				<a href="`+url+`/set-password?cod=`+docs[0]._id+`">Recuperar Contraseña</a>
				`, "Recupera tu Contraseña")

				return res.redirect('/recuperar-password?msg=Te hemos  enviado un correo revísalo para continuar con el proceso')
			}
			else
			{
				return res.redirect('/recuperar-password?msg=Usuario no encontrado')
			}

		})
	},

	getSetPassword:function(req,res,next){
		usuario_model.find({_id:req.query.cod}, function(err,docs){
			return res.render('public/setPass',{
				title:"Ingresar Nuevo Pass", 
				msg:req.query.msg,
				usuario:docs[0]
			})
		})
	},

	postSetPassword : function(req,res,next){
		if(req.body.password == req.body.password1)
		{
			
			var salt = bcrypt.genSaltSync(10);
			pass = bcrypt.hashSync(req.body.password, salt);
			cod=req.body.cod
			usuario_model.update({_id:cod},{password:pass},function(err,docs){
				return res.redirect('/login?msg=tu password fué restablecido, ¡Inicia Sesión!')
			})
		}
		else
		{
			return res.redirect('/set-password?cod='+req.body.cod+'&msg=Los Password no Coinciden')
		}
	}, 
	getValidarDatos:function(req, res, next){
		// usuario_model.find({fid:req.user.fid}, function(err,docs){
		// 	return  res.render('admin/validarDatos')
		// })
		return  res.render('admin/validarDatos', {user:req.user})
	}, 

	postValidarDatos: function(req,res, next){
		console.log(req.body)
		req.user.completed="1"
		req.body.completed="1"
		console.log(req.user)
		usuario_model.update({fid:req.user.fid},req.body,function(err,docs){
			console.log(err)
			return res.redirect('/panel')
		})
		
	}

}

// { "_id" : ObjectId("5dd7ec95d569d7790c01dedf"), "tipo" : "2", "confirmacion" : "1", "activo" : "1", "nombre" : "HEREDIA LOPEZ MATILDE GUADALUPE ", "correo" : "pekemilicha1983@hotmail.com", "celular" : "0987460857", 
// "ci" : "1803876984", "ciudad" : "Ambato", "cod" : "4foaitiq", "password" : "$2a$10$xsvAOESgdy9likPmhAjacung5D4Nbmgi443u4jUIktLgLb4GJmKhm", "f_creacion" : ISODate("2019-11-22T14:11:33.191Z"), "__v" : 0, "f_oscus" : I
// SODate("2002-06-18T00:00:00Z") }

// db.usuarios.update({"_id" : ObjectId("5dd7ec95d569d7790c01dedf")},{
// 	"_id" : ObjectId("5dd7ec95d569d7790c01dedf"), "tipo" : "1", "confirmacion" : "1", "activo" : "1", "nombre" : "HEREDIA LOPEZ MATILDE GUADALUPE ", "correo" : "pekemilicha1983@hotmail.com", "celular" : "0987460857", 
// "ci" : "1803876984", "ciudad" : "Ambato", "cod" : "4foaitiq", "password" : "$2a$10$xsvAOESgdy9likPmhAjacung5D4Nbmgi443u4jUIktLgLb4GJmKhm", "f_creacion" : ISODate("2019-11-22T14:11:33.191Z")
// })