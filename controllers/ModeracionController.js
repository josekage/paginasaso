var mysql = require('mysql');
var sanitizer = require('sanitizer');
const sgMail = require('@sendgrid/mail');
var bcrypt = require('bcryptjs');
//cambiar URL para produccion
// var url="http://192.168.0.103"
var url="http://anuncios.oscus.coop"
function send_mail(mail, mensaje){
sgMail.setApiKey('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');
const msg = {
  to: mail,
  from: 'info@oscus.coop',
  subject: 'OSCUS CLASIFICADOS',
  text: "Notificación",
  html: mensaje,
};

sgMail.send(msg);
}



var producto_model=require('../models/producto')
var usuario_model=require('../models/usuario')
var categoria_model=require('../models/categoria')

var notify = require('../controllers/NotificacionController')
module.exports={
	getPanelModeracion: function(req,res,next){

		producto_model.find({publicado:"1", activo:"1"}, function(err,docs){
			categoria_model.populate(docs,{path:"categoria"}, function(err2,docs2){
				usuario_model.populate(docs2,{path:"vendedor"},function(err3,docs3){
					return res.render('moderacion/panel',{productos:docs3,isAuthenticated: req.isAuthenticated(),
						user: req.user,})
				})
			})
		})
	}, 
	getDescripcionProducto: function(req,res,next){
		var id = sanitizer.escape(req.params.id_producto);

		producto_model.find({publicado:"0", _id:id}, function(err,docs){
			categoria_model.populate(docs,{path:"categoria"}, function(err2,docs2){
				usuario_model.populate(docs2,{path:"vendedor"},function(err3,docs3){
					return res.render('moderacion/producto',{producto:docs3[0],isAuthenticated: req.isAuthenticated(),
						user: req.user,})
				})
			})
		})

	}, 
	ApiAprobar: function(req,res,next){
		var id = sanitizer.escape(req.body.id_producto);
		producto_model.update({_id:id},{publicado:"1"}, function(err,docs){
			if(err)
			{
				return res.json("error")
			}
			else
			{
				//send_mail(req.body.correo, "Tu producto fue aprobado y ya se encuentra publicado en nuestra plataforma")
				notify.ProductoAprobado(req.body.correo)
				return res.json('success')
			}
		})
	},
	ApiRechazar: function(req,res,next){
		var id = sanitizer.escape(req.body.id_producto);
		producto_model.update({_id:id},{publicado:"0", activo:"0"}, function(err,docs){
			console.log(docs)
			if(err)
			{
				return res.json("error")
			}
			else
			{
				// send_mail(req.body.correo, "Lo sentimos, tu producto no fue aprobado, revisa nuestras politicas de uso ")
				notify.ProductoRechazado(req.body.correo)
				return res.json('success')
			}
			
		})

		// var config = require('.././database/config');
		// //Restriccion a una sola consulta SQL 
		// config.multipleStatements= false;

		// var db = mysql.createConnection(config);
		// var id = sanitizer.escape(req.body.id_producto);
		// parametro={
		// 	id_producto:id
		// }
		// update={
		// 	estado:2
		// }
		// db.query('update producto set ? where ?',[update, parametro], function(err,rows){
		// 	if(err)
		// 	{
		// 		db.end();
		// 		console.log(err);
		// 		return res.json("error")
		// 	}
		// 	else
		// 	{
		// 		db.end();
		// 		send_mail(req.body.correo, "Lo sentimos, tu producto no fue aprobado, revisa nuestras politicas de uso ...")
		// 		return res.json('success')
		// 	}

		// })
	},

	getNuevoUsuario:function(req,res,next){
		return res.render('moderacion/nuevo_usuario',{isAuthenticated: req.isAuthenticated(),
			user: req.user,})
	},

	postNuevoUsuario : function(req,res,next){
		
		var cod = (Date.now() - Date.parse("January 01, 2017")).toString(32);
		
		var salt = bcrypt.genSaltSync(10);
		var password = bcrypt.hashSync(req.body.ci, salt);
		req.body.password=password
		req.body.tipo="2"

		onj = new usuario_model(req.body)
		onj.save(function(err,doc){
			send_mail(req.body.correo_usuario, "Hola "+req.body.nombre+", ya estás registrado en los Clasificados de OSCUS, tu clave provisional es: <b>"+req.body.ci+"</b> recuerda cambiarla  ")
			return res.redirect('/nuevo-usuario')
		})		
		

	},

	getListadoUsuarios:function(req,res,next){

		usuario_model.find({confirmacion:"1"}, function(err,docs){
			return res.render('moderacion/usuarios',{
				isAuthenticated: req.isAuthenticated(),
				user: req.user,
				usuarios:docs
			})
		})

	},

	getEditarUsuario : function(req,res,next){
		usuario_model.find({_id:req.query.cod},function(err,docs){
			if(docs)
			{
				return res.render('moderacion/EditarUsuario',{
					user:req.user,
					usuario:docs[0]
				})
			}
			else
			{
				return res.redirect('/usuarios')
			}
			
		})
	},

	postEditarUsuario : function(req,res,next){
		console.log(req.body)
		cod= req.body.cod
		delete req.body.cod
		
			usuario_model.update({_id:cod},req.body,function(err,docs){
				return res.redirect('/usuarios-editar?cod='+req.body.cod)
			})

		
	},

	getActivarUsuario: function(req,res,next){

		usuario_model.update({_id:req.query.cod},{activo:"1"}, function(err,docs){
			usuario_model.find({_id:req.query.cod}, function(err2,docs2){
				send_mail(docs2[0].correo,`
				<h2>Felicidades tu cuenta ya esta activa, recuerda tu contraseña es tu numero de cédula</h2>
				<p>Para iniciar sesión ingresa en el siguiente enlace</p>
				<a href="`+url+`/login">Iniciar Sesión</a>
				`)
				return res.redirect('/usuarios-editar?cod='+req.query.cod)
			})
			
		})
	},

	getDesactivarUsuario : function(req,res,next){
		usuario_model.update({_id:req.query.cod},{activo:"0"}, function(err,docs){
			return res.redirect('/usuarios-editar?cod='+req.query.cod)
		})
	},
	getNoSocio: function(req,res,next)
	{
		id=req.query.id
		usuario_model.find({_id:id},function(err,docs){
			notify.NoSocio(docs[0].correo)
			usuario_model.update({_id:id},{activo:"3"}, function(err1,docs2){
				return res.redirect('/usuarios')
			})
			
		})
		
	},
	getInfoDesactualizada: function(req,res,next)
	{
		id=req.query.id
		usuario_model.find({_id:id},function(err,docs){
			notify.CuentaNoVerificadaXDatos(docs[0].correo)
			return res.redirect('/usuarios')
		})
		
	}
}

