var mysql = require('mysql');
var bcrypt = require('bcryptjs');
//var express = require('express');
var formidable = require('express-formidable');
var producto_model=require('../models/producto')
var categoria_model=require('../models/categoria')
var usuario_model=require('../models/usuario')
var pregunta_model=require('../models/pregunta')
var respuesta_model=require('../models/respuesta')
var favorito_model=require('../models/favorito')
var mongoose = require('mongoose');
module.exports = {
	panel: function (req, res, next) {
		var pendientes=[]

			producto_model.find({vendedor:req.user.id}, function(err2,productos){
				
				if(productos)
				{
					_arr_productos=[]
					productos.forEach(function(el){
						_arr_productos.push(el._id)
					})
					pregunta_model.find({producto:{ $in : _arr_productos}},function(err,preguntas){
						console.log(preguntas)
					respuesta_model.find({producto:{$in:_arr_productos}},function(err3, respuestas){

						preguntas.forEach(function(pregunta){
							var flag=0
							respuestas.forEach(function(respuesta){
									if(respuesta.pregunta==pregunta._id)
									{
										//si hay respuesta 
									
										flag++
									}
							})
							if(flag==0)
							{
								pendientes.push(pregunta)
							}
						})

						console.log(preguntas)
						console.log(respuestas)
						return res.render('admin/panel', {
							isAuthenticated: req.isAuthenticated(),
							user: req.user,
							preguntas:pendientes,
							productos:productos
						});

					})
				})
				}
			
		})
		
	},
	logout: function (req, res, next) {
		req.logout();
		res.redirect('/');
	},
	perfil: function (req, res, next) {
		return res.render('admin/perfil', {
			isAuthenticated: req.isAuthenticated(),
			user: req.user
		});
	},
	postActualizarPerfil: function (req, res, next) {
		
		console.log(req.body)

		if (!req.body.foto_usuario) {
			var usuario = {
				nombre: req.body.nombre_usuario,
				ci: req.body.ci_usuario,
				celular: req.body.telefono_usuario,
				ciudad: req.body.ciudad_usuario,
				negocio:{
					nombre:req.body['negocio[nombre]'],
					actividad:req.body['negocio[actividad]'],
					telefono:req.body['negocio[telefono]'],
					correo:req.body['negocio[correo]'],
					lat:req.body['negocio[lat]'],
					lng:req.body['negocio[lng]'],
				}
			};

		} else {
			var usuario = {
				nombre: req.body.nombre_usuario,
				ci: req.body.ci_usuario,
				telefono: req.body.telefono_usuario,
				ciudad: req.body.ciudad_usuario,
				foto: req.body.foto_usuario,
				negocio:{
					nombre:req.body['negocio[nombre]'],
					actividad:req.body['negocio[actividad]'],
					telefono:req.body['negocio[telefono]'],
					correo:req.body['negocio[correo]'],
					lat:req.body['negocio[lat]'],
					lng:req.body['negocio[lng]'],
				}
			};
		}

		
		console.log(usuario)
		usuario_model.update({_id:req.user.id},usuario,function(err,docs){
			
			if(req.body.foto_usuario)
			{
				req.user.foto=usuario.foto
			}
			
			return res.json("sucesss");
			
		})
		// console.log(usuario);
		// //obtener  los estudiantes 
		// db.query('UPDATE `usuario` SET ?  WHERE ?', [usuario, {
		// 	id_usuario: req.user.id
		// }], function (err, col, fields) {
		// 	if (err)
		// 		console.log(err);
		// 	else {
		// 		return res.json("sucesss");
		// 	}

		// });
	},
	getNuevoProducto: function (req, res, next) {
		return res.render('admin/nuevo-producto', {
			isAuthenticated: req.isAuthenticated(),
			user: req.user
		})
	},
	getMisProductos: function (req, res, next) {
		producto_model.find({vendedor:req.user.id, activo:"1"}, function(err,docs){
			categoria_model.populate(docs,{path:"categoria"},function(err2,docs2){
				console.log(docs2)
				return res.render('admin/mis_productos', {
					isAuthenticated: req.isAuthenticated(),
					user: req.user,
					productos: docs2
				});
			})
			
		})

	},
	getFavoritos : function(req, res,next)
	{
		favorito_model.find({usuario:req.user.id}, function(err,docs){
			producto_model.populate(docs, {path:"producto"}, function(err2, favoritos){
				if(!favoritos)
				{
					favoritos=[]
				}
				return res.render('admin/favoritos', {
					isAuthenticated: req.isAuthenticated(),
					user: req.user,
					productos: favoritos
				});
			})
		})
		// var config = require('.././database/config');
		// var db = mysql.createConnection(config);
		// db.connect();
		// db.query('SELECT * FROM favoritos inner join producto on producto_favorito = id_producto WHERE ?', {
		// 	usuario_favoritos: req.user.id
		// }, function (err, col, fields) {
		// 	if (err) {
		// 		console.log(err);
		// 		db.end();
		// 	} else {
		// 		console.log(col);
		// 		return res.render('admin/favoritos', {
		// 			isAuthenticated: req.isAuthenticated(),
		// 			user: req.user,
		// 			productos: col
		// 		});
		// 	}

		// });
	
	}, 
	getQuitarFavoritos:function(req,res,next){
		favorito_model.remove({_id:req.query.cod}, function(err,docs){
			return res.redirect("/favoritos")
		})
	},
	getResponder : function(req,res,next){
		console.log("Responder")
		pregunta_model.find({_id:req.query.id},function(err,docs){
			return res.render('admin/responder', {
				isAuthenticated: req.isAuthenticated(),
				user: req.user, 
				pregunta:docs[0]
			})
		})
	}, 
	postResponder: function(req,res,next){

		obj = new respuesta_model(req.body)
		obj.save(function(){
			return res.redirect('/panel')
		})
	},

	getDashBoard:function(req,res,next){
		// return res.render('admin/dashboard',{
		// 	user:req.user
		// })
		var productos_activos=0;
		var productos_pendientes=0

		var usuarios_activos=0
		var usuarios_pendientes=0
		var usuarios_inactivos=0

		producto_model.find({activo:"1", publicado:"1"}, function(err,docs){
			productos_activos=docs.length;
			producto_model.find({activo:1, publicado:"0"}, function(err1,docs1){
				productos_pendientes=docs1.length
				producto_model.find({activo:"1", publicado:"1"},{},{
					limit:10,
					sort:{
						visitas: -1 //Sort by Date Added DESC
					}}, function(err2,docs2){

						usuario_model.find({confirmacion:"1"}, function(err3,docs3){

							docs3.forEach(function(el){
								if(el.activo=="0")
								{
									usuarios_pendientes++
								}
								if(el.activo=="1")
								{
									usuarios_activos++
								}
								if(el.activo=="3")
								{
									usuarios_inactivos++
								}
							})

							return res.render('admin/dashboard',{
								activos:productos_activos,
								pendientes:productos_pendientes,
								top:docs2, 
								usuarios_activos:usuarios_activos,
								usuarios_inactivos:usuarios_inactivos,
								usuarios_pendientes:usuarios_pendientes,
								user:req.user
							})


						})

						
					})
			})
		})
	}
}