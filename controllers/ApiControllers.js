var mysql = require('mysql');
var formidable = require('formidable');
var bcrypt = require('bcryptjs');
var fs = require('fs');
var categoria_model = require('../models/categoria')
var slug = require('slug')
var sanitizer = require('sanitizer');
var notify = require('../controllers/NotificacionController')
const sgMail = require('@sendgrid/mail');
const sharp = require('sharp');


function send_mail(mail, mensaje){
sgMail.setApiKey('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');
const msg = {
  to: mail,
  from: 'info@oscus.coop',
  subject: 'OSCUS CLASIFICADOS',
  text: "Notificación",
  html: mensaje,
};

sgMail.send(msg);
}

var producto_model=require('../models/producto')
var favorito_model=require('../models/favorito')
var pregunta_model=require('../models/pregunta')
var usuario_model=require('../models/usuario')
var resena_model=require('../models/resena')

module.exports = {

	postSubirImagen: function (req, res, next) {

		console.log("Se ingreso al modulo de subida");
		var subir = new formidable.IncomingForm();
		var ruta = "/imagenes/usuarios/";
		console.log(subir);
		subir.uploadDir = 'public/images/usuarios';
		subir.parse(req);
		subir.on('error', function (err) {
			console.log("HUbo un error");
			console.log(err);
		});
		
		subir.on('end', function (field, file) {
			console.log("Subido Correctamente");
			console.log(file);
		});
		subir.on('fileBegin', function (field, file) {
			if (file.name) {
				var ext;
				if (file.type == 'image/png') {
					ext = ".png"
				}
				if (file.type == 'image/jpeg') {
					ext = ".jpeg"
				}
				if (file.type == 'image/jpg') {
					ext = ".jpg"
				}

				file.path += ext;
				var a = file.path.split('public', 2);
				ruta = a[1];
				console.log("la ruta es ");
				console.log(ruta);
				return res.json(ruta);
				//return ruta;
			}
		});
		subir.on('file', function (field, file) {
			console.log('Archivo recibido');
		});



	},
	postSubirImagenProducto_OLD: function (req, res, next) {

		console.log("Se ingreso al modulo de subida");
		var subir = new formidable.IncomingForm();
		var ruta = "/imagenes/productos/";
		console.log(subir);
		subir.uploadDir = 'public/images/productos';
		subir.parse(req);
		subir.on('error', function (err) {
			console.log("HUbo un error");
			console.log(err);
		});
		
		subir.on('end', function (field, file) {
			console.log("Subido Correctamente");
			// console.log(file);
		});
		subir.on('fileBegin', function (field, file) {
			if (file.name) {
				var ext;
				if (file.type == 'image/png') {
					ext = ".png"
				}
				if (file.type == 'image/jpeg') {
					ext = ".jpeg"
				}
				if (file.type == 'image/jpg') {
					ext = ".jpg"
				}
				if (file.type == 'image/jpeg') {
					ext = ".jpeg"
				}

				file.path += ext;
				var a = file.path.split('public', 2);
				ruta = a[1];
				console.log("la ruta es ");
				console.log(ruta);
				return res.json(ruta);
				//return ruta;
			}
		});
		subir.on('file', function (field, file) {
			console.log('Archivo recibido');
		});


	},
	postSubirImagenProducto:function(req,res,next){
	
		var subir = new formidable.IncomingForm();
		var ruta = "/img/original/";
		subir.uploadDir = 'public/img/original';
		subir.parse(req);
		subir.on('error', function (err) {
			console.log("HUbo un error");
			console.log(err);
		});
		
		subir.on('end', function (field, file) {
			console.log(ruta)
			console.log("iniciando Conversión");
			// una vez terminado de subir la imagen
			// 	procedemos a redimensionar
			var nombre=ruta.split("/")[3]
            sharp(process.cwd()+'/public'+ruta)
                    .resize({ height: 200 })
                    .toFile(process.cwd()+'/public/img/small/'+nombre,(err,inf)=>{
						sharp(process.cwd()+'/public'+ruta)
						.resize({ height: 600 })
						.toFile(process.cwd()+'/public/img/medium/'+nombre,(err,inf)=>{
							return res.json({
								original:ruta,
								medium:'/img/medium/'+nombre, 
								small:'/img/small/'+nombre, 
							 });
						})	
					})
                    
                
				// return ruta;
			
        });
        subir.on('file', function (field, file) {
			console.log('Archivo recibido');
		});
		subir.on('fileBegin', function (field, file) {
			if (file.name) {
				var ext;
				if (file.type == 'image/png') {
					ext = ".png"
				}
				if (file.type == 'image/jpeg') {
					ext = ".jpeg"
				}
				if (file.type == 'image/jpg') {
					ext = ".jpg"
				}
				if (file.type == 'application/pdf') {
					ext = ".pdf"
				}

				file.path += ext;
				var a = file.path.split('public', 2);
				ruta = a[1];
				console.log("la ruta es ");
                console.log(ruta);
                
                
			}
		});
		
	//fin de subida

	},
	postCategorias: function (req, res, next) {
		var cpadre = req.body.id;
		// var config = require('.././database/config');
		// config.multipleStatements=false
		// var db = mysql.createConnection(config);
		// db.connect();

		// db.query('SELECT * FROM `categoria2` WHERE ?;', {
		// 	c2_padre: cpadre
		// }, function (err, col, fields) {
		// 	if (err) {
		// 		console.log(err);
		// 		return res.json("error");
		// 	} else {
		// 		return res.json(col);
		// 	}
		// });
		// cpadre= parseInt(cpadre)
		console.log(req.body)
		categoria_model.find({indice_padre:cpadre},function(err,docs){
			console.log(err)
			console.log(docs)
			return res.json(docs)
		})
	},
	postBorrarImagenProducto: function (req, res, next) {
		//var prev =".././public";
		var path = req.body.ruta;
		console.log(path);
		fs.readFile(path, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				console.log(data);
			}
		});
		fs.exists(path, function (a1) {
			console.log(a1);
			if (a1) {
				fs.unlink(path, function (err) {
					if (err) {
						return res.json("error");
					} else {
						return res.json("success");
					}
				});
			} else {
				console.log("No Existe");
			}
		})

	},
	postCrearProducto: function (req, res, next) {
		
		var cadena= sanitizer.escape(req.body.nombre)
		var fotos=[]
		if(req.body.fotos)
		{
			fotos=JSON.parse(req.body.fotos)
		}
		cadena=slug(cadena)
		producto2={
			nombre: sanitizer.escape(req.body.nombre),
			categoria: sanitizer.escape(req.body.categoria),
			// foto_1: sanitizer.escape(req.body.f1),
			// foto_2: sanitizer.escape(req.body.f2),
			// foto_3: sanitizer.escape(req.body.f3),
			// foto_4: sanitizer.escape(req.body.f4),
			fotos:fotos,
			vendedor: sanitizer.escape(req.user.id),
			descripcion:sanitizer.escape(req.body.descripcion),
			precio: sanitizer.escape(req.body.precio),
			slug:cadena
		}

		producto_model.find({slug:cadena},function(err,docs){
			if(docs.length>0)
			{
				console.log("Se repite el slug")
				n_slug=cadena+" "+docs.length
				
				cadena=slug(n_slug)
				producto2.slug=cadena
				obj = new producto_model(producto2)
				obj.save(function(err,docs){
					if(err)
					{
						return res.json("error")
					}
					else{
						//send_mail(req.user.email, "Gracias por publicar con nosotros, tu producto está siendo revisado en breve será publicado")
						
						console.log("******************** ENVIANDO NOTIFICACION DESDE API *******************")
						// notify.ProductoRecibido(req.user.email)
						return res.json('success')
					}
				})
			}

			else
			{
				obj = new producto_model(producto2)
				obj.save(function(err,docs){
					if(err)
					{
						return res.json("error")
					}
					else{
						//send_mail(req.user.email, "Gracias por publicar con nosotros, tu producto está siendo revisado en breve será publicado")
						console.log("******************** ENVIANDO NOTIFICACION DESDE API *******************")
						// notify.ProductoRecibido(req.user.email)
						return res.json('success')
					}
				})
			}
			
			

		})
		
		
	},
	postEliminarProducto: function (req, res, next) {
		var id = req.body.id;
		producto_model.update({_id:id},{activo:"0"}, function(err,docs){
			if(err)
			{
				return res.json("error")
			}
			else
			{
				return res.json("success")
			}
		})
	},
	postProductos: function (req, res, next) {
		var config = require('.././database/config');
		config.multipleStatements=false
		var db = mysql.createConnection(config);
		db.connect();
		// si se envia un parametro de categoria 
		if (req.body.c2) 
		{
			db.query('SELECT * FROM `producto` WHERE ? order by fecha_publicacion desc limit 15;', {
				c2: req.body.c2
			}, function (err, col, fields) {
				if (err) {
					console.log(err);
					return res.json("error");
				} else {
					return res.json(col);
				}

			});
		}
		else
		{
			db.query('SELECT * FROM `producto` order by fecha_publicacion desc limit 15', function (err, col, fields) {
				if (err) {
					console.log(err);
					return res.json("error");
				} else {
					return res.json(col);
				}

			});
		}

	},
	postInfoProducto: function(req,res,next){
		var id = req.body.id;
		var config = require('.././database/config');
		config.multipleStatements=false
		var db = mysql.createConnection(config);
		db.connect();

		db.query('SELECT * FROM `producto` WHERE ?', {
			id_producto: id
		}, function (err, col, fields) {
			if (err) {
				console.log(err);
				return res.json("error");
			} else {
				return res.json(col);
			}

		});
	},
	postNuevoFavorito : function(req,res,next){

		var id = req.body.id;
		var favorito ={
			usuario : req.user.id,
			producto : id
		}
		obj = new favorito_model(favorito)
		favorito_model.find(favorito, function(err,docs){
			if(err)
			{
				return res.json("error")
			}
			console.log(docs)
			if(docs.length==0)
			{
				obj.save(function(err2,docs2){
					return res.json("success")
				})
				
			}
			else
			{
				return res.json("repeat")
				
			}
		})
		// var config = require('.././database/config');
		// config.multipleStatements=false
		// var db = mysql.createConnection(config);
		// db.connect();

		// db.query('INSERT INTO favoritos SET ?;', favorito, function (err, col, fields) {
		// 	if (err) {
		// 		console.log(err);
		// 		db.end();
		// 		return res.json("error");
				
		// 	} else {
		// 		db.end();
		// 		return res.json("success");
		// 	}

		// });
	}, 
	
	postNuevaPregunta : function(req,res,next)
	{
		
		_producto=req.body.producto
		req.body.usuario= req.user;
		req.body.id_usuario= req.user.id;

		nombre_pregunta  = req.user.nombre;
		console.log(req.body)
		obj= new pregunta_model(req.body)
		obj.save(function(){
			producto_model.find({_id:_producto}, function(err,docs1){
				usuario_model.populate(docs1,{path:"vendedor"}, function(err2,docs){
					console.log(docs)
					send_mail(docs[0].vendedor.correo, nombre_pregunta+", hizo una pregunta en tu producto: "+docs[0].nombre+", ve a tu tablero para revisarlo")
					return res.redirect('/producto/'+docs[0].slug)
				})
				
			})
			
		})
		// var config = require('.././database/config');
		// config.multipleStatements=false
		// var db = mysql.createConnection(config);
		// db.connect();
		// req.body.usuario_pregunta= req.user.id;
		// db.query('INSERT INTO pregunta set ?;',req.body, function (err, col, fields) {
		// 	if (err) {
		// 		console.log(err);
		// 		return res.redirect('/producto/'+req.body.producto_pregunta)
		// 	} else {
		// 		return res.redirect('/producto/'+req.body.producto_pregunta)
		// 	}

		// });
	},

	postNuevaResena : function(req,res,next){
		var obj= new resena_model({
			propietario: req.body.vendedor,
			evaluador: req.user.id,
			descripcion:req.body.comentario,
			puntuacion:req.body.calificacion
		})
		obj.save(function(){
			return res.redirect('/')
		})
	}, 

	getStats: function(req,res,next){
		console.log("Generando Estadisticas")
		producto_model.find({vendedor:req.user.id, publicado:"1", activo:"1"},{_id:0,publicado:0,activo:0}, function(err,docs){
			return res.json(docs)
		})
	},

	getApiContacto: function(req,res,next){
		producto_model.update({_id:req.query.id},{ $inc: { contacto: 1} },function(err9,incre){
			return res.json("success")
		})
	},

	getGlobalStats:function(req,res,next){
		var productos_activos=0;
		var productos_pendientes=0

		var usuarios_activos=0
		var usuarios_pendientes=0
		var usuarios_inactivos=0

		producto_model.find({activo:"1", publicado:"1"}, function(err,docs){
			productos_activos=docs.length;
			producto_model.find({activo:1, publicado:"0"}, function(err1,docs1){
				productos_pendientes=docs1.length
				producto_model.find({activo:"1", publicado:"1"},{},{
					limit:10,
					sort:{
						visitas: -1 //Sort by Date Added DESC
					}}, function(err2,docs2){

						usuario_model.find({confirmacion:"1"}, function(err3,docs3){

							docs3.forEach(function(el){
								if(el.activo=="0")
								{
									usuarios_pendientes++
								}
								if(el.activo=="1")
								{
									usuarios_activos++
								}
								if(el.activo=="3")
								{
									usuarios_inactivos++
								}
							})

							return res.json({
								activos:productos_activos,
								pendientes:productos_pendientes,
								top:docs2, 
								usuarios_activos:usuarios_activos,
								usuarios_inactivos:usuarios_inactivos,
								usuarios_pendientes:usuarios_pendientes
							})


						})

						
					})
			})
		})
	}
		
}