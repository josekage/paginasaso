module.exports = {

	isLogged : function(req, res, next){
		if(req.isAuthenticated()){
			next();
		}else{
			res.redirect('/login');
		}
	},

	isAdmin: function(req,res,next){
		if(req.isAuthenticated()){
			if(req.user.tipo==1){
				next();
			}
			else
			{
				res.redirect('/');
			}
		}
		else
		{
			return res.redirect('/');
		}
	},

	isNotLogin : function(req,res,next){
		if(req.user)
		{
			res.redirect('/panel')
		}
		else
		{
			next()
		}
	},

	isNotCompleted :function(req,res,next)
	{
		if(req.user.completed=="1")
		{
			next()
		}
		else{
			res.redirect('/validar-datos')
		}
	}

}